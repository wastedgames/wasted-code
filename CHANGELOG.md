# Changelog
This file documents notable changes to Wasted across versions since its inception.

It should be kept in mind that some versions have a less complete changelog than others, and that this changelog do not list the details of the many small bugfixes and improvements which together make a significant part of the progress between releases.

For similar reasons, and because some features are vastly more complex than others, attributions of main changes should not be taken as a shortcut for overall contribution.

## Wasted 0.6.0-alpha (15. February 2025)

This new release finally implements the radio feature, an ARM64 Linux build.. and much more

**ICONS**
* update: #79 - replace all overworld dialog modal icons and add new icon for options
* update: help icon in paused-game modal #79
* update: pause menu icons replaced with new ones

**MAPS**
* Add new Skatepark map by Blenderbumbass

**FREEROAM**
* add: speed counter gui to overworld gamemode

**CAMERA**
* update: gamepad camera rotation using setters
* update: gamepad camera rotation implementation
* update: implement proper orbital camera
* update: new lateral offset implementation when car turns, fix bug that constantly tried to re-align camera

**REBRANDING**
* fix: stk occurences in INSTALL file
* fix: ask internet access message #79

**CARS**
* add: goat to official karts
* delete: blade-xtr car from official karts

**TUTORIAL**
* fix: nitro probability for tutorial gamemode
* fix: tutorial red circle spawns bowling balls

**AUDIO**
* fix: initialize audio to 0.4 volume, too many people complained about it being too loud

**SFX**
* add: new nitro sounds and on/off logic as in issue #5 wastedgames/wasted-assets#5 of asset repository
* fix: skid pitch sound now related to kart speed
* fix: nitro sounds not playing properly online
* fix: #91 by adding logic to allow horn sound to play continously
* fix: reduce minimum rpm engine sound for every car
* update: horn sound play method
* Add new Explosion sound

**MUSIC**
* Add 2 new radio stations

**VISUALS**
* Add motion blur

**BUGFIXING**
* fix: always use default car instead of hardcoded tux or 173rx
* fix: default to ipv4 because on windows apparently dual_stack is not supported. Needs to be turned on conditionally in the future
* fix: use release build for windows, show sha256 for integrity checks
* fix: remove b flag which prevented HOSTS.txt file to be hashed on windows
* update: windows_builder script to make it run better and automate more stuff
* fix: #99, comment tabs in register screen in gui file and set always register screen to local account tab
* add: method to get random double
* add: time parameter to set message duration
* fix: car change. Trigger car change method from server too fixing inconsistency
* fix: try to change minimum time to use powerup to fix client prediction problems
* update: simplify logic for powerup usage a bit in kart class, remove unused fire clicked var
* fix: podium objects not showing up if the track has some objects which are not podium ones and fixed logic to load podium if there are no objects
* fix: #88 allow spectators to change camera at the end of the game and look at podium
* fix: wrong kart types
* fix: issue #65
* fix: always print HPs when an online match starts
* fix: removed obscure, stupid logic for setOnScreenText method, initialize pointers for text objects to nullptr

**RELEASE**
* fix: #80 enable ARM64 build
* update: don't build supertuxkarteditor anymore

**RADIO**
* add: implement new radio station logic 
* fix: don't throw exception if a music file was not found since we now default to radio music rather then default music
* update: refactor music manager methods and move logic from track class to music manager
* del: remove default music
* add: clearRadioHistory method, some vars cleanup
* fix: limit radio tuning to 1 second interval
* fix: game crash when switching off music and changing radio
* fix: startMusic() being called too many times from worldStatus and world classes. Initialize music only from world classes during go phase and override it in overworld. Don't call music_manager but initialize radio stations from Track class
* add: implement random seek when radio starts or when radio station is changed
* initialize radiostation beacklog array
* fix: start music only once when loading overworld, from its own class. Use the world_status class call for the other game modes
* add: implementation to resume radio music playback while switching radio station
* add: lastchangedstation time tracking and radio stations history
* update: save current music when playing track
* update: fetch music duration when music starts
* add: method to retrieve music duration, initalized to 0
* add: method to return normal music
* add: seek command to allow music to be played at specific point in time
* update: save cur_time value in ogg file class
* add: different logic to draw radio names depending on race or overworld init
* update: don't draw radio names if there is some track music
* update: add draw radio station name method to GUI classes, allow render global to put the message in the queue only once and call method when button is pressed
* add: keybindings for keyboard and gamepad; bind buttons to paddle keys if available for gamepads otherwise don't assign them
* add: keybindings for next/previous radio station navigation
* update: implement methods to initialize radio array, choose an initial random radio station when music manager is initialized, implement next/previous radio stations methods, play static noise when changing radio
* add: implementation for radio stations, removed default track defaulting to radio station music, allow specific map music to override radio music too
* add: new paths to radio stations, radio station array with enum which specifies station names

**TESTING**
* add: new radio stations automatic tests

## Wasted 0.5.0-alpha (15. September 2024) - The FATTEST release ever!

This release is PACKED with new features, new cars and bug fixes! What are you waiting for?
Try it already!

**BUGFIXING**
* Fix Regression preventing cycle camera (7bb7303086)
* #85: tutorial segfaulting after enabling network
* Fix icons for make install (4485b69726)
* Fix some typos in release scripts
* Fix logo icon not showing in credits menu

**TESTING**
* Added tests for derby mode

**CAMERA**
* Follow car's acceleration and deceleration and turning (1aaf626e24)
* Check camera free mode state with boolean (97288ffb98)
* Don't follow car's roll with camera 
* Camera distance adjustments for bigger or smaller cars (ef75be7c41)
* Camera distance can now be adjusted also using personalized settings in the menu

**PHYSICS**
* Add 3 new cars classes (van, minicar, muscle), updated the current ones (citycar, sport)
* Tweaked car characteristics for better physics
* Cars don't fly in the air anymore if they hit a wall
* Angular velocity is not reset anymore while cars fly so jumps/falls are much more realistic now (c1502858af)

**VISUALS**
* Add car braking lights that light on if the car brakes(A car can support up to 4)

**TUTORIAL**
* Add a basic tutorial to the game
* Add tutorial tips

**REBRANDING**
* Fix window's name (98043fcb30) 
* Changed .git file to -> wasted.git (2d6c8e6724)
* Fixed some old name "supertuxkart" mismatching in options menu and other places in-game

**MISC**
* Bump minimum Cmake ver to 3.6.0 (057178f9e8)
* Fix README
* Updated News Manager so now can fetch news from outside sources (0d17156cb1)
* Add missing reset_camera and cycle_camera actions for script engine (59807cc600)
* Differentiate different tips based on game mode, load them during loading screen too (b617e4cee4)
* Remove display message part in Free for all server class, as it is only relevant for client logic
* New implementation to support both UI messages without queue or queued 

**MULTIPLAYER**
* Add HOSTS.txt file to tell the game where should it look for multiplayer servers. Check shasum to be sure that official server list stays unchanged

**UI**
* Changed menu background
* Changed theme font
* Changed menu icons
* Disable trophies icons in free roam mode (ee12464807) 
* Add ability to load any image for background screens, add a new loading screen bg (9b2fa36261)
* New font classes copying what the bold one is doing, using this dirty hack to fix the blurred upscaled large font which is just too bad (a1165abc5e)

**CLEANUP**
* Removed some unused methods in ffa-server
* Removed unused wip-tracks
* Removed unused music
* Removed unused 
* Removed Xue

**CARS**
* Add 2 new cars (KEI,DAJIBAN)
* Fix 222RS color not changeable 

**TRACKS**
* Add sun to derby track

**MUSIC**
* Add 3 new music tracks
* Adjusted music volumes properly
* Updated licenses
* Added new menu theme
* Added new Wasted radio jingle

**SFX**
* Fixed handbrake sound not being positional

This took a long while to write :O
It's amazing how many things managed to fit into one release :') 

## Wasted 0.4.0-alpha (15. May 2024) - Biggest release ever!

With this new release we added new features and fixed many bugs. I truly believe this was the 
biggest release ever and required the highest effort.

**CAMERA**
* Fix camera bug: reset kart position when camera is reinitialized
* Add limit to zoom in/out
* Bring back smooth camera option and add the flag to disable/enable in the menu
* Fix default camera rotation speed value
* Add ability to rotate camera with gamepad sticks
* Add new camera type "PODIUM"

**VISUALS**
* Fixed: particles not displayed properly
* Add: new gfx to add tires burnout to skidding
* Add: show smoke effect and tires skidmark when handbraking
* Add: default podium for tracks that don't have one

**PHYSICS**
* Add: handbrake logic to allow car to handbrake without drifting
* Fix: car physics params
* Fix: calculate damage based on mass and not car class
* Add: more car classes
* Fix: impulse when cars collide

**MISC**
* Update: rename executable to Wasted (Xavier Del Campo)
* Fix: #69 prevent users to click on custom server selection if fetching is not complete
* Fix: #74 camera not switching to podium after player changes camera target
* Fix: #76 don't fetch camera when game is in menu state and gamepad is connected
* Fix: #78 iterating through all karts after countdown ends produced leaderboard bug
* Fix: tire position in widgets (select kart for example)
* Add: random float generator
* Fixed bug #64 that appeared to make a player pick up the same powerup twice when triggering the red circle

**TRACKS**
* Update: first derby track with some new additions and fixed terrain clipping

**KARTS**
* Fix: tires position
* Fix: exhausts position

**MUSIC**
* Add: new music

**SFX**
* Update: changed skid sound

Thanks to everyone who contributed to this release. Let's keep up the amazing work!

## Wasted 0.3.0-alpha (31. December 2023) - (Almost) Happy new Year!

With this release we added a ton of **things**!
We also had our first real test session with 4 players that actually allowed me to fix some nasty bugs.

* Fixed smoke not being removed after car is fixed if car has health < 25 #52
* Fixed driving backwards gives other players HPs #60
* Reassign cycle/reset camera buttons for gamepads to avoid error during compilation (f1414eb011)
* Added a Windows build! (Thanks Chris for the help) #56
* Add script to facilitate creation of portable windows build
* Format numbers properly in final score screen #57
* Add podium at the end of the match the shows the first 3 players in order 
* Cars were only represented with default color on the podium #59
* Allow players to fetch LAN servers if not logged in, WAN servers if logged in with a STK account (Will be removed in the future)
* Fix wrong height for name and HP of the player
* Fix ordering of in-game players #58
* Some code cleanup related to #48

**SFX**

* Added new car fix sfx
* Added new zombie powerup sfk
* Replaced menu car selection sfx
* Replaced menu start race sfx

**TRACKS**

* Added new default free roam map (Blenderdumbass)
* Added podium to default map

## Wasted 0.2.0-alpha (01. October 2023)

We are happy to announce the second Wasted release!
This release is also dedicated to SpongyCake. He won't be actively contributing anymore but we thank him for all the huge work done so far.
Hope to see your contributions again someday!

This release has huge changes! I am having trouble writing this changelog so something may be missing :D

* Separated FFA server code from client code - spongycake (b5e125e241)
* Fixed resized window camera rotation bug - Rampoina (3acf6e0b83)
* Huge changes to score system. Now calculated adding HPs to played time
* Free camera mode with camera rotation and in-game camera change!
* New version of 173-rx-mk2 car - Crystaldaevee
* Players eliminated by falling down are not entirely removed from the game - franzbonanza (d68596ec23)
* Add smoke effect to players with damaged cars (<25 HPs) - franzbonanza (8f3159303a)
* Eliminate people who are AFK (don't kick them) - franzbonanza (d704aab76a)
* Fix explosions for everybody playing (previously they were buggy)
* Don't remove cars when they explode in the map
* Fixed spectator camera not being assigned automatically to alive players
* Fix camera rotation not being calculated properly - franzbonanza (bb4568b98a)
* Add button to reset camera position and rotation - franzbonanza (a24969fda2)
* Changed how damages are handled: now we also check for car class (medium, light or heavy) - franzbonanza(a09e7f6dd2)
* Fix car preview in menu - franzbonanza (92a99eba89)
* Change make variables - spongycake (e96e44d8ad)
* Add  camera zoom in/out - franzbonanza (4d8cd3e432)
* Start tutorial precondition gui_tour - spongycake (5404cb4c9f)

### Non-game related:

* Documentation updates


## Wasted 0.1.0-alpha (18. May 2023)

We are Happy to announce our first alpha release!
With this release the game finally reached a playing state.

* added 3 official cars(173-rx-mk2, 222RS, blade-xtr) and 1 official map (sky grid)
* add derby game mode
* add damage system
* changed music and added music playlists to game maps
* add server filter to show only compatible servers
* changed menu UI to access to online matches easily
* 6 new powerups
* added new help manual

### Non-game related:

* new official master server up to easily host matches at any time
* added appimage script to automatically create appimages and improved linux builder script
* server protocol version set to 0


