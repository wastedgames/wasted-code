#include "karts/kart_model.hpp"
#include "modes/free_for_all_server.hpp"
#include "modes/world_with_rank.hpp"
#include "states_screens/race_gui_base.hpp"
#include "tracks/track_object.hpp"

#include <vector>

class NetworkString;

class DerbyTest : public FreeForAllServer
{

protected:


public:
    DerbyTest();
// ------------------------------------------------------------------------
static void testNormalDerbyMatchLeaderboard();
static void testCountdownReachedZeroLeaderboard();
static void resetState();
// ------------------------------------------------------------------------

};
