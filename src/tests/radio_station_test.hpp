#include "audio/music_manager.hpp"

class NetworkString;

class RadioStationTest : public MusicManager
{

protected:


public:
    RadioStationTest();
// ------------------------------------------------------------------------
static void testRadioMusicIsLoadedCorrectly();
static void testChangeRadioStationSuccessfully();
static void testPlayRadioAndTestSeekWhenChangingRadioStation();

static void resetState();
// ------------------------------------------------------------------------

};
