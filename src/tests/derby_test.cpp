#include "tests/derby_test.hpp"
#include "config/user_config.hpp"
#include "karts/abstract_kart.hpp"
#include "config/player_manager.hpp"
#include "network/network_config.hpp"
#include "network/rewind_manager.hpp"
#include "tracks/track.hpp"
#include "tracks/track_manager.hpp"


#include <iostream>
// ----------------------------------------------------------------------------

void DerbyTest::testNormalDerbyMatchLeaderboard()
{
    DerbyTest::resetState();
    NetworkConfig::get()->setIsServer(true);
    RaceManager::get()->setTimeTarget(500);
    RaceManager::get()->setMajorMode(RaceManager::MAJOR_MODE_SINGLE);
    RaceManager::get()->setMinorMode(RaceManager::MINOR_MODE_FREE_FOR_ALL);
    RaceManager::get()->setNumKarts(3);
    RaceManager::get()->setNumPlayers(3,3);

    // ADD CARS
    for(int i = 0; i < 3; i++){
        StateManager::get()->createActivePlayer(
            PlayerManager::getCurrentPlayer(),
            NULL);
        RaceManager::get()->setPlayerKart(i, UserConfigParams::m_default_kart);
    }
    RaceManager::get()->setupPlayerKartInfo();
    RaceManager::get()->setTrack("derby_track");


    // START RACE
    RaceManager::get()->startNew(false);
    FreeForAllServer* f = dynamic_cast<FreeForAllServer*>(FreeForAllServer::getWorld());
    f->setPhase(RACE_PHASE);

    auto k = f->getKarts();

    k[0]->setEnable(true);
    k[1]->setEnable(true);
    k[2]->setEnable(true);

    // DO DAMAGE TO CARS
    k[0].get()->setHp(40);
    k[1].get()->setHp(85);


    Log::info("Print Kart 0 Life after damage dealt", "'%f'.",
              k[0].get()->getHp());
    Log::info("Print Kart 1 Life after damage dealt", "'%f'.",
              k[1].get()->getHp());

    // SET  HP TO 0 IF YOU ARE GOING TO ELIMINATE
    f->setTime(300);
    k[0].get()->setHp(0);
    k[0]->finishedRace(300);
    f->m_last_player_time = 300;
    f->eliminateKart(0);
    f->calculateScore(0);

    f->setTime(200);
    k[1].get()->setHp(0);
    k[1]->finishedRace(200);
    f->m_last_player_time = 200;
    f->eliminateKart(1);
    f->calculateScore(1);

    // END RACE
    f->isRaceOver();

    Log::info("Print Phase:", "'%i'.",
              FreeForAllServer::getWorld()->getPhase());

    Log::info("Print Time:", "'%d'.",
              FreeForAllServer::getWorld()->getTime());

    // CHECK SCORE
    Log::info("Kart 0 Score", "'%d'.",
              f->getKartScore(0));
    Log::info("Kart 1 Score", "'%d'.",
              f->getKartScore(1));
    Log::info("Kart 2 Score", "'%d'.",
              f->getKartScore(2));

    if(
        (f->getKartScore(0) != 200) ||
        (f->getKartScore(1) != 300) ||
        (f->getKartScore(2) != 400)
        ){
        Log::error("ERROR", "Score value different from one expected.\n"
                            " Expected: 200,300,400"
                            " Your logic sucks!");
        assert(false);
    }
}

// ----------------------------------------------------------------------------

void DerbyTest::testCountdownReachedZeroLeaderboard()
{

    DerbyTest::resetState();
    NetworkConfig::get()->setIsServer(true);
    RaceManager::get()->setTimeTarget(500);
    RaceManager::get()->setMajorMode(RaceManager::MAJOR_MODE_SINGLE);
    RaceManager::get()->setMinorMode(RaceManager::MINOR_MODE_FREE_FOR_ALL);
    RaceManager::get()->setNumKarts(3);
    RaceManager::get()->setNumPlayers(3,3);

    // ADD CARS
    for(int i = 0; i < 3; i++){
        StateManager::get()->createActivePlayer(
            PlayerManager::getCurrentPlayer(),
            NULL);
        RaceManager::get()->setPlayerKart(i, UserConfigParams::m_default_kart);
    }
    RaceManager::get()->setupPlayerKartInfo();
    RaceManager::get()->setTrack("derby_track");


    // START RACE
    RaceManager::get()->startNew(false);
    FreeForAllServer* f = dynamic_cast<FreeForAllServer*>(FreeForAllServer::getWorld());

    f->setPhase(RACE_PHASE);

    auto k = f->getKarts();

    k[0]->setEnable(true);
    k[1]->setEnable(true);
    k[2]->setEnable(true);

    // DO DAMAGE TO CARS
    k[0].get()->setHp(55);
    k[1].get()->setHp(33);

    Log::info("Print Kart 0 Life after damage dealt", "'%f'.",
              k[0].get()->getHp());
    Log::info("Print Kart 1 Life after damage dealt", "'%f'.",
              k[1].get()->getHp());

    // SET  HP TO 0 IF YOU ARE GOING TO ELIMINATE
    f->setTime(233);
    k[0].get()->setHp(0);
    Log::info("Print Kart 0 Life after it was killed", "'%f'.",
              k[0].get()->getHp());
    k[0]->finishedRace(233);
    f->m_last_player_time = 233;
    f->eliminateKart(0);
    f->calculateScore(0);

    // COUNTDOWN REACHES 0
    f->setTime(0);
    f->setPhase(RESULT_DISPLAY_PHASE);
    f->countdownReachedZero();

    // END RACE
    f->isRaceOver();

    Log::info("Print Phase:", "'%i'.",
              FreeForAllServer::getWorld()->getPhase());

    Log::info("Print Time:", "'%d'.",
              FreeForAllServer::getWorld()->getTime());

    // CHECK SCORE
    Log::info("Kart 0 Score", "'%d'.",
               f->getKartScore(0));
    Log::info("Kart 1 Score", "'%d'.",
              f->getKartScore(1));
    Log::info("Kart 2 Score", "'%d'.",
              f->getKartScore(2));

    if(
    (f->getKartScore(0) != 267) ||
    (f->getKartScore(1) != 533) ||
    (f->getKartScore(2) != 600)
        ){
        Log::error("ERROR", "Score value different from one expected.\n"
                            " Expected: 267,533,600"
                            " Your logic sucks!");
        assert(false);
    }
}

void DerbyTest::resetState(){
    STKProcess::reset();
    StateManager::clear();
    NetworkConfig::clear();
    RaceManager::clear();
    RaceManager::destroy();
    RaceManager::create();
    if(Track::getCurrentTrack()){
    Track::getCurrentTrack()->cleanup();
    }
    RewindManager::destroy();
}
