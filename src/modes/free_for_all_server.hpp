//  SuperTuxKart - a fun racing game with go-kart
//  Copyright (C) 2018 SuperTuxKart-Team
//
//  This program is free software; you can redistribute it and/or
//  modify it under the terms of the GNU General Public License
//  as published by the Free Software Foundation; either version 3
//  of the License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef FREE_FOR_ALL_SERVER_HPP
#define FREE_FOR_ALL_SERVER_HPP

#include "modes/free_for_all.hpp"
#include "states_screens/race_gui_base.hpp"

#include <vector>

class NetworkString;

class FreeForAllServer : public FreeForAll
{

protected:
    bool m_enabled_network_spectator;
    bool m_count_down_reached_zero;
    std::vector<int> m_scores;
    // ------------------------------------------------------------------------
    void handleKartHitInServer(int kart_id, int hitter);
    // ------------------------------------------------------------------------
    void setKartHps(int kart_id, int hpAmount);


private:
    // ------------------------------------------------------------------------
    virtual video::SColor getColor(unsigned int kart_id) const;

public:
    // ------------------------------------------------------------------------
    FreeForAllServer();
    unsigned int killed_karts = 0;
    float m_last_player_time = 0;
    // ------------------------------------------------------------------------
    virtual ~FreeForAllServer();
    // ------------------------------------------------------------------------
    virtual void init() OVERRIDE;
    // ------------------------------------------------------------------------
    virtual bool isRaceOver() OVERRIDE;
    // ------------------------------------------------------------------------
    virtual void reset(bool restart=false) OVERRIDE;
    // ------------------------------------------------------------------------
    virtual void getKartsDisplayInfo(
                 std::vector<RaceGUIBase::KartIconDisplayInfo> *info) OVERRIDE;
    // ------------------------------------------------------------------------
    virtual bool raceHasLaps() OVERRIDE                       { return false; }
    // ------------------------------------------------------------------------
    virtual const std::string& getIdent() const OVERRIDE;
    // ------------------------------------------------------------------------
    virtual bool kartHit(int kart_id, int hitter = -1) OVERRIDE;
    // ------------------------------------------------------------------------
    virtual bool setKartHpsPowerup(int kart_id, int hpAmount = 0) OVERRIDE;
    // ------------------------------------------------------------------------
    virtual void update(int ticks) OVERRIDE;
    // ------------------------------------------------------------------------
    void calculateScore(int kart_id);
    //-------------------------------------------------------------------------
    virtual void countdownReachedZero() OVERRIDE;
    // ------------------------------------------------------------------------
    virtual void terminateRace() OVERRIDE;
    // ------------------------------------------------------------------------
    virtual void eliminateKart(int kart_number, bool notify_of_elimination = true) OVERRIDE;
    // ------------------------------------------------------------------------
    void setKartScoreFromServer(NetworkString& ns);
    // ------------------------------------------------------------------------
    void setKartTextAndUpdateScore(NetworkString& ns);

    int getKartScore(int kart_id) const        { return m_scores.at(kart_id); }
    // ------------------------------------------------------------------------
    bool getKartFFAResult(int kart_id);
    // ------------------------------------------------------------------------
    virtual std::pair<uint32_t, uint32_t> getGameStartedProgress() const
        OVERRIDE;
    // ------------------------------------------------------------------------
    virtual void addReservedKart(int kart_id) OVERRIDE
    {
        WorldWithRank::addReservedKart(kart_id);
        m_scores.at(kart_id);
    }
    // ------------------------------------------------------------------------
    void handleDisconnectInServer(int kart_id);
    // ------------------------------------------------------------------------
    virtual void saveCompleteState(BareNetworkString* bns,
                                   STKPeer* peer) OVERRIDE;
    // ------------------------------------------------------------------------
    virtual void restoreCompleteState(const BareNetworkString& b) OVERRIDE;
};   // FreeForAllServer


#endif
